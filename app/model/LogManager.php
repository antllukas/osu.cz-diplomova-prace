<?php

namespace App\Model;

use Nette;


/**
 * LokaceManager.
 */
class LogManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'log_aktivita',
		COLUMN_ID = 'idlog_aktivita';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	//tohle je funkce která mi vypíče celou tabulku uzivatel
	public function getAll5Prihlaseni()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('log_aktivita.*, uzivatel.image, uzivatel.jmeno, uzivatel.prijmeni')
			->where('aktivita', 'Přihlášení')
            ->order('datetime DESC')
            ->limit(5);
	}

	public function getAll5Registrace()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('log_aktivita.*, uzivatel.image, uzivatel.jmeno, uzivatel.prijmeni')
			->where('aktivita', 'Registrace')
            ->order('datetime DESC')
            ->limit(5);
	}

	public function getAll5Odhlaseni()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('log_aktivita.*, uzivatel.image, uzivatel.jmeno, uzivatel.prijmeni')
			->where('aktivita', 'Odhlášení')
            ->order('datetime DESC')
            ->limit(5);
	}

	public function get5PoslednichMesicu()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('Month(`datetime`) AS mesic, count(*) AS pocet, aktivita')
			->group('mesic')
            ->order('datetime DESC')
            ->where('aktivita')
            ->limit(5);
	}

	public function get5PoslednichMesicuPocetPrihlaseni()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('Month(`datetime`) AS mesic, count(*) AS pocet, aktivita')
			->group('mesic, aktivita')
            ->order('datetime DESC')
            ->where('aktivita', 'Přihlášení')
            ->limit(5);
	}

	public function get5PoslednichMesicuPocetOdhlaseni()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('Month(`datetime`) AS mesic, count(*) AS pocet, aktivita')
			->group('mesic, aktivita')
            ->order('datetime DESC')
            ->where('aktivita', 'Odhlášení')
            ->limit(5);
	}


	public function get5PoslednichMesicuRegistrace()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('Month(`datetime`) AS mesic, count(*) AS pocet, aktivita')
			->group('mesic')
            ->order('datetime DESC')
            ->where('aktivita', 'Registrace')
            ->limit(5);
	}




	public function insertLog($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
	}




}

