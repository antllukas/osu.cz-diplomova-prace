<?php

namespace App\Model;

use Nette;


/**
 * CertificateManager.
 */
class CertificateManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'certifikat',
		COLUMN_ID = 'id_certifikatu';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	//tohle je funkce která mi vypíče celou tabulku uzivatel
	public function getAllCertificate()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('certifikat.*, ucastnik.jmeno, ucastnik.prijmeni')
            ->order('datum ASC');
	}

	public function getAllCertificateCount()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('certifikat.*')
            ->order('datum ASC')->count();
	}


	public function getAllCertificateById($id)
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('certifikat.*, ucastnik.jmeno, ucastnik.prijmeni')
			->where('uzivatel_id_ucastnik', $id)
            ->order('datum ASC');
	}


	public function getCertificateById($id)
	{
		return $this->database->table(Self::TABLE_NAME)
			->select(Self::TABLE_NAME.'.*')
			->where(Self::COLUMN_ID, $id)->fetch();
	}


	public function maVygenerovanyCertifikat($id, $idbeh)
	{
		return $this->database->table(Self::TABLE_NAME)
			->where('uzivatel_id_ucastnik = ? AND beh_id = ?', $id, $idbeh)->fetch();
	}


	public function insertCertifikat($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
			
	}	


	

}

