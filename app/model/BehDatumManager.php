<?php

namespace App\Model;

use Nette;


/**
 * BehDatumManager.
 */
class BehDatumManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'beh_datum',
		COLUMN_ID = 'idbeh_datum';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}



	public function insertBehDatum($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
	}	

	public function updateBehDatum($id, $hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}	


	public function getBehDatumById($id)
	{
		return $this->database->table(Self::TABLE_NAME)
            ->where('beh_beh_id', $id);
	}

	public function getBehDatumAll()
	{
		return $this->database->table(Self::TABLE_NAME)
		->select('beh_datum.*');
	}

	public function getCenaKurzu($id)
	{
		return $this->database->table('beh')
		->where('beh_id', $id)
		->select('castka')->fetch();
	}

	public function getNazevKurzu($id)
	{
		return $this->database->table('beh')
		->where('beh_id', $id)
		->select('beh.castka, kurz.nazev')->fetch();
	}


	public function getBehDatumAllByUser($id)
	{
		return $this->database->table('ucastnik_has_beh')
		->select('ucastnik_has_beh.*')
		->where('uzivatel_id_ucastnik', $id);
	}

	public function deleteBehDatum($id)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->delete();
	}	


	

}

