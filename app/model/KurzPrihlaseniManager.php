<?php

namespace App\Model;

use Nette;


/**
 * KurzPrihlaseniManager.
 */
class KurzPrihlaseniManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'ucastnik_has_beh',
		COLUMN_ID = 'ucastnik_has_beh_id';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}



	public function insertKurzPrihlaseni($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
	}	

	public function jePrihlasen($iducastnika, $idbehu)
	{
			return $this->database->table(Self::TABLE_NAME)
			->select('*')
			->where('uzivatel_id_ucastnik = ? AND beh_beh_id = ? AND lektor = 0', $iducastnika, $idbehu)->fetch();
	}	


	public function uzivateleFromKurzById($idbehu)
	{
			return $this->database->table(Self::TABLE_NAME)
			->select(Self::TABLE_NAME.'.*, uzivatel.*')
			->where('beh_beh_id = ? AND lektor = 0', $idbehu);
	}	


	public function jeLektor($iducastnika, $idbehu)
	{
			return $this->database->table(Self::TABLE_NAME)
			->select('*')
			->where('uzivatel_id_ucastnik = ? AND beh_beh_id = ? AND lektor = 1', $iducastnika, $idbehu)->fetch();
	}	

	public function jeNekdeLektor($iducastnika)
	{
			return $this->database->table(Self::TABLE_NAME)
			->select('*')
			->where('uzivatel_id_ucastnik = ? AND lektor = 1', $iducastnika)->fetch();
	}	




	public function updateKurzPrihlaseni($id, $hodnoty)
	{
			
	}	

	public function deleteKurzPrihlaseni($id, $iducastnika)
	{
			$this->database->table(self::TABLE_NAME)->where('beh_beh_id = ? AND uzivatel_id_ucastnik = ?', $id, $iducastnika)->delete();
	}	

	public function getPrihlaseniById($id)
	{
		return $this->database->table(self::TABLE_NAME)
			->where(self::COLUMN_ID, $id);
	}

	public function getPrihlaseniByIdKurz($id)
	{
		return $this->database->table(self::TABLE_NAME)
			->where('beh_beh_id', $id);
	}



	

}

