<?php

namespace App\Model;

use Nette;


/**
 * AdresaManager.
 */
class AdresaManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'uzivateladresa',
		COLUMN_ID = 'id_ucastnikAdresa';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	//tohle je funkce která mi vypíče celou tabulku uzivatel




	public function insertAdresu($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
	}	

	public function getAdresaByUserId($id)
	{
		return $this->database->table(self::TABLE_NAME)
		    ->select(self::TABLE_NAME.'.*')
			->where('uzivatel_id_ucastnik = ', $id);
	}


	public function deleteAdresa($id)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->delete();
	}	

	public function updateAdresa($id, $hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}	


}

