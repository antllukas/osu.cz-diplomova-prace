<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'uzivatel',
		COLUMN_ID = 'id_ucastnik',
		COLUMN_NAME = 'username',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_EMAIL = 'email',
		COLUMN_AKTIVNI = 'aktivni',	
		COLUMN_JMENO = 'jmeno',	
		COLUMN_PRIJMENI = 'prijmeni',			
		COLUMN_ROLE = 'role';


	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		$row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_NAME, $username)->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException('Uživatelské jméno neexistuje', self::IDENTITY_NOT_FOUND);

		} elseif ($row['aktivni'] == 0) {
			
			throw new Nette\Security\AuthenticationException('Nemáte schválený účet!', self::INVALID_CREDENTIAL);

		} elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException('Špatně zadané heslo!', self::INVALID_CREDENTIAL);

		} elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
			$row->update([
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
			]);
		}

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}


	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @param  string
	 * @return void
	 * @throws DuplicateNameException
	 */
	public function add($username, $email, $password, $jmeno, $prijmeni)
	{
		try {
			$this->database->table(self::TABLE_NAME)->insert([
				self::COLUMN_NAME => $username,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
				self::COLUMN_EMAIL => $email,
				self::COLUMN_JMENO => $jmeno,
				self::COLUMN_PRIJMENI => $prijmeni,
				self::COLUMN_AKTIVNI => 0,
				self::COLUMN_ROLE => 'Uživatel',
			]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

	//tohle je funkce která mi vypíče celou tabulku uzivatel
	public function getAllUsers()
	{
		return $this->database->table(self::TABLE_NAME)
            ->order(self::COLUMN_ID . ' DESC');
	}

	public function getCountAllUsers()
	{
		return $this->database->table(self::TABLE_NAME)
            ->select('*')->count();
	}

	public function getCountCekajiciSchvalit()
	{
		return $this->database->table(self::TABLE_NAME)
            ->select('*')
            ->where('aktivni', 0)->count();
	}


	public function getAllUsersNotActive()
	{
		return $this->database->table(self::TABLE_NAME)
			->where('aktivni', 0)
            ->order(self::COLUMN_ID . ' DESC');
	}	

	public function getUserById($id)
	{
		return $this->database->table(self::TABLE_NAME)
		    ->select('uzivatel.*')
			->where(self::COLUMN_ID, $id)
            ->fetch();
	}

	public function getUserByUserName($username)
	{
		return $this->database->table(self::TABLE_NAME)
		    ->select('uzivatel.*')
			->where('username', $username)
            ->fetch();
	}


	public function getParametrById($parametr, $id)
	{
		return $this->database->table(self::TABLE_NAME)
		    ->select(self::TABLE_NAME.'.'.$parametr)
			->where(self::COLUMN_ID, $id)
            ->fetch();
	}

	public function getLastId(){
		return $this->database->table(self::TABLE_NAME)
		    ->select('id_ucastnik')
			->order('id_ucastnik DESC')
			->limit(1)
            ->fetch();
	}



	public function insertUzivatel($hodnoty)
	{
			$id = $this->database->table(self::TABLE_NAME)->insert($hodnoty);
			return $id;
	}

	public function updateUzivatel($id, $hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}	

	public function updateConfirmUser($id)
	{		
		    $hodnoty = new Nette\ArrayHash;
			$hodnoty->aktivni = 1;
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}	

	public function updateNoConfirmUser($id)
	{		
		    $hodnoty = new Nette\ArrayHash;
			$hodnoty->aktivni = 2;
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}	



}



class DuplicateNameException extends \Exception
{}
