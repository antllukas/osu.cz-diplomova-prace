<?php

namespace App\Model;

use Nette;


/**
 * LokaceManager.
 */
class TelefonManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'uzivatel_telefon',
		COLUMN_ID = 'id_telefon';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function insertTelefon($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
	}

	public function getTelefonByUserId($id)
	{
		return $this->database->table(self::TABLE_NAME)
		    ->select(self::TABLE_NAME.'.*')
			->where('uzivatel_id_ucastnik = ', $id);
	}


	public function deleteTelefon($id)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->delete();
	}	

	public function updateTelefon($id, $hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}		



}

