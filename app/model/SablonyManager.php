<?php

namespace App\Model;

use Nette;


/**
 * SablonyManager.
 */
class SablonyManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'kurz',
		COLUMN_ID = 'id_kurz';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getAllSablony()
	{
		return $this->database->table(Self::TABLE_NAME)
            ->order('nazev ASC');
	}

	public function getAllSablonyCount()
	{
		return $this->database->table(Self::TABLE_NAME)
            ->order('nazev ASC')->count();
	}


	public function getSablonuById($id)
	{
		return $this->database->table(Self::TABLE_NAME)
            ->where(Self::COLUMN_ID, $id)->fetch();

	}

	public function insertSablonu($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
	}	

	public function updateSablony($id, $hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->where(Self::COLUMN_ID, $id)->update($hodnoty);
	}			

	public function deleteSablonu($id)
	{
			$this->database->table(self::TABLE_NAME)->where(Self::COLUMN_ID, $id)->delete();
	}

}

