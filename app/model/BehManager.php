<?php

namespace App\Model;

use Nette;


/**
 * LokaceManager.
 */
class BehManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'beh',
		COLUMN_ID = 'beh_id';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getLastId(){
		$uzivatel_id = $this->database->getInsertId('beh');
		return $uzivatel_id;
	}


	public function getBehById($id)
	{
		return $this->database->table(Self::TABLE_NAME)
			->select(Self::TABLE_NAME.'.*, kurz.*')
            ->where(Self::COLUMN_ID, $id)->fetch();
	}

	public function getAllBehy()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('beh.*, kurz.nazev')
            ->order('rok ASC');
	}


	public function getAllBehyNeukoncene()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('beh.*, kurz.nazev')
			->where('konec_kurzu > NOW()')
            ->order('rok ASC');
	}

	public function getAllBehyUkoncene()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('beh.*, kurz.nazev')
			->where('konec_kurzu < NOW()')
            ->order('rok ASC');
	}



	public function getPocetAktivnichBehu()
	{
		return $this->database->table(Self::TABLE_NAME)
			->select('beh.*')
			->where('`zacatek_kurzu` < NOW() AND `konec_kurzu` > NOW()')->count();
	}

	public function getPocetLidiVBehu($id)
	{
		return $this->database->table('ucastnik_has_beh')
			->select('ucastnik_has_beh.*')
			->where('beh_beh_id = ? AND lektor = 0', $id)->count();
	}

	public function deleteBeh($id)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->delete();
	}	



	public function insertBeh($hodnoty)
	{
			$id_behu = $this->database->table(self::TABLE_NAME)->insert($hodnoty);
			return $id_behu;
	}	

	

	public function updateBeh($id, $hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}	

}

