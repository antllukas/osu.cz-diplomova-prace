<?php

namespace App\Model;

use Nette;


/**
 * LokaceManager.
 */
class LokaceManager
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'lokace',
		COLUMN_ID = 'id_lokace';


	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	//tohle je funkce která mi vypíče celou tabulku uzivatel
	public function getAllLokace()
	{
		return $this->database->table(Self::TABLE_NAME)
            ->order('nazev ASC');
	}

	public function getAllLokaceCount()
	{
		return $this->database->table(Self::TABLE_NAME)
            ->order('nazev ASC')->count();
	}


	public function getLokaceById($id)
	{
		return $this->database->table(Self::TABLE_NAME)
            ->where(Self::COLUMN_ID, $id)->fetch();

	}

	public function inserLokace($hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->insert($hodnoty);
	}	

	public function updateLokace($id, $hodnoty)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->update($hodnoty);
	}


	public function deleteLokaci($id)
	{
			$this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->delete();
	}	

}

