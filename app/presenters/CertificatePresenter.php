<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\CertificateManager;
use \DotBlue\Mpdf\DocumentFactory;


class CertificatePresenter extends BasePresenter
{

	/**
     * @inject
     * @var \DotBlue\Mpdf\DocumentFactory
     */
    public $documentFactory;

	/** @var CertificateManager */
    private $CertificateManager;

    public function __construct(CertificateManager $CertificateManager)
    {
        $this->CertificateManager = $CertificateManager;
    }

    public function handleGenerovatPDF($id){
    	$certifikat = $this->CertificateManager->getCertificateById($id);
    	$iduzivatel = $certifikat->uzivatel_id_ucastnik;

    	$clovek = $this->UserManager->getUserById($iduzivatel);
    	$test['jmeno'] = $clovek->jmeno;
    	$test['prijmeni'] = $clovek->prijmeni;
    	$test['hlavni_nazev'] = $certifikat->hlavni_nazev;
    	$test['datum'] = $certifikat->datum;
    	$test['garant'] = $certifikat->garant;

    	$invoiceDocument = $this->documentFactory->createPdf('certifikat', 'default.latte', $test);
    	$invoiceDocument->printPdf();
    }

	public function renderDefault()
	{
		if($this->user->getIdentity()->role == "Administrátor"){
		$this->template->certifikaty = $this->CertificateManager->getAllCertificate();
		}else{
		$id = $this->user->getIdentity()->id;
		$this->template->certifikaty = $this->CertificateManager->getAllCertificateById($id);
		}
	}




	public function startup()
	{
		parent::startup();
		if($this->getUser()->isLoggedIn()){

		}else{
			$this->redirect("Sign:in");
		}
	}




}
