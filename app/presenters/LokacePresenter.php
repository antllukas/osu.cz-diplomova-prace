<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\LokaceManager;
use Nette\Application\UI\Form;


class LokacePresenter extends BasePresenter
{

	/** @var LokaceManager */
    private $LokaceManager;

    public function __construct(LokaceManager $LokaceManager)
    {
        $this->LokaceManager = $LokaceManager;
    }


	public function renderDefault()
	{
		$this->template->lokace = $this->LokaceManager->getAllLokace();
	}

    public function renderUpravit($id)
    {
        
    }

    public function handleSmazatLokaci($id){
        $this->LokaceManager->deleteLokaci($id);
        $this->redirect('Lokace:');
    }


	public function startup()
	{
		parent::startup();
		if($this->getUser()->isLoggedIn()){
            if (!$this->user->isAllowed($this->name, $this->action)){
                $this->flashMessage('Nemáš přístup do úpravy učeben!', 'alert alert-danger');
                $this->redirect("Homepage:");
            }
		}else{
			$this->redirect("Sign:in");
		}
	}

	protected function createComponentNewLokaceForm()
	{
		$form = new Form;
        $form->addText('nazev', 'Název Lokace:')
        	 ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
        	 ->setAttribute('req required', 'required');

        $form->addText('ulice', 'Ulice:')
        	 ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
        	 ->setAttribute('req required', 'required');   

        $form->addText('cislo_popisne', 'Číslo popisné:')
        	 ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
        	 ->setAttribute('req required', 'required');     

        $form->addText('mesto', 'Město:')
        	 ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
        	 ->setAttribute('req required', 'required');        

        $form->addText('psc', 'PSČ:') 
        	 ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('data-inputmask','\'mask\': \'999 99\'')
        	 ->setAttribute('req required', 'required');
        $form->addSubmit('pridat', 'Přidat objekt');
        $form->onSuccess[] = [$this, 'newLokaceFormSucceeded'];
        return $form;
	}

    public function newLokaceFormSucceeded(Form $form, $values)
    {
    	$this->LokaceManager->inserLokace($values);
        $this->flashMessage('Učebna byla vytvořena!', 'alert alert-success');
        $this->redirect('Lokace:');
    }

    protected function createComponentEditLokaceForm()
    {
        $lokace = $this->LokaceManager->getLokaceById($this->getParam("id"));
        $form = new Form;
        $form->addText('nazev', 'Název Lokace:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($lokace->nazev);

        $form->addText('ulice', 'Ulice:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($lokace->ulice);  

        $form->addText('cislo_popisne', 'Číslo popisné:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($lokace->cislo_popisne);     

        $form->addText('mesto', 'Město:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($lokace->mesto);       

        $form->addText('psc', 'PSČ:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setAttribute('data-inputmask','\'mask\': \'999 99\'')
             ->setDefaultValue($lokace->psc);

        $form->addHidden('id', $lokace->id_lokace);    


        $form->addSubmit('pridat', 'Upravit objekt');
        $form->onSuccess[] = [$this, 'editLokaceFormSucceeded'];
        return $form;
    }	

    public function editLokaceFormSucceeded(Form $form, $values)
    {
        $id = $values->id;
        unset($values->id);
        $this->LokaceManager->updateLokace($id, $values);
        $this->flashMessage('Učebna byla upravena!', 'alert alert-success');
        $this->redirect('Lokace:');
    }


}
