<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\BehDatumManager;


class CalendarPresenter extends BasePresenter
{

	private $BehDatumManager;

    public function __construct(BehDatumManager $BehDatumManager)
    {
        $this->BehDatumManager = $BehDatumManager;
    }


	public function renderDefault()
	{
		if($this->user->getIdentity()->role == "Administrátor"){
		$this->template->behy = $this->BehDatumManager->getBehDatumAll();
		$this->template->zapsano = 123456789;
		}else{
		$this->template->behy = $this->BehDatumManager->getBehDatumAll();
		$id = $this->user->getIdentity()->id;
		$this->template->zapsano = $this->BehDatumManager->getBehDatumAllByUser($id);
		}
		
	}


	public function startup()
	{
		parent::startup();
		if($this->getUser()->isLoggedIn()){

		}else{
			$this->redirect("Sign:in");
		}
	}

	public function getCenaKurzu($id){
		return $this->BehDatumManager->getCenaKurzu($id);
	}
	public function getNazevKurzu($id){
		return $this->BehDatumManager->getNazevKurzu($id);
	}

	public function getUrlKurzu(){
		return "../hodiny/informace/";
	}





}
