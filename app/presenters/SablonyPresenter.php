<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\SablonyManager;
use Nette\Application\UI\Form;


class SablonyPresenter extends BasePresenter
{

	/** @var LokaceManager */
    private $SablonyManager;

    public function __construct(SablonyManager $SablonyManager)
    {
        $this->SablonyManager = $SablonyManager;
    }


	public function renderDefault()
	{
	   $this->template->sablony = $this->SablonyManager->getAllSablony();
	}

    public function renderUpravit($id)
    {
        
    }

    public function handleSmazatSablonu($id){
        $this->SablonyManager->deleteSablonu($id);
        $this->redirect('Sablony:');
    }




	public function startup()
	{
		parent::startup();
		if($this->getUser()->isLoggedIn()){
            if (!$this->user->isAllowed($this->name, $this->action)){
                $this->flashMessage("Nemáš přístup!");
                $this->redirect("Homepage:");
            }
		}else{
			$this->redirect("Sign:in");
		}
	}


    protected function createComponentNewSablonyForm()
    {
        $form = new Form;
        $form->addText('nazev', 'Název kurzu:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required');

        $form->addTextArea('kratky_popis', 'Kártký popis:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setAttribute('req required', 'required');

        $form->addTextArea('popis', 'Detailní popis:')
             ->setAttribute('req required', 'required')
             ->setAttribute('id', 'editor-textu');  

        $form->addTextArea('sylab_popis', 'Popis sylabu:')
        ->setAttribute('req required', 'required')
        ->setAttribute('id', 'editor-textu2');  

        $form->addTextArea('cil_predmetu', 'Cíl předmětu:')
        ->setAttribute('req required', 'required')
        ->setAttribute('id', 'editor-textu3');                    

        $form->addTextArea('obsah', 'Obsah:')
        ->setAttribute('req required', 'required')
        ->setAttribute('id', 'editor-textu4');  
 
        $form->addTextArea('garanti_vyucujici_popis', 'Garanti vyučující popis:')
        ->setAttribute('req required', 'required')
        ->setAttribute('id', 'editor-textu5');

        $form->addTextArea('literatura', 'Literatura:')
        ->setAttribute('req required', 'required')
        ->setAttribute('id', 'editor-textu6');

        $form->addTextArea('pozadavky_na_studenta', 'Požadavky na studenta:')
        ->setAttribute('req required', 'required')
        ->setAttribute('id', 'editor-textu7');

        $form->addText('pracoviste_zkratka', 'Zkratka pracoviště:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required');  

        $form->addText('rozsah_hodin', 'Rozsah hodin:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setAttribute('data-inputmask','\'mask\': \'99 - 99\'');  

$zakonceni_kurzu = [
    'Zápočet' => 'Zápočet',
    'Zkouška' => 'Zkouška',
];

$form->addSelect('zakonceni_kurzu', 'Zakončení kurzu:', $zakonceni_kurzu)
    ->setAttribute('class', 'form-control')
    ->setAttribute('req required', 'required')
    ->setPrompt('Vyberte zakončení kurzu...');

$vyucovaci_metoda = [
    'Prezenční' => 'Prezenční',
    'Kombinovaná' => 'Kombinovaná',
    'Dálková' => 'Dálková',    
];

$form->addSelect('vyucovaci_metoda', 'Vyučovací metoda:', $vyucovaci_metoda)
    ->setAttribute('class', 'form-control')
    ->setAttribute('req required', 'required')
    ->setPrompt('Vyberte vyučovací metodu...');    


        $form->addSubmit('pridat', 'Vytvořit šablonu kurzu');
        $form->onSuccess[] = [$this, 'newSablonyFormSucceeded'];
        return $form;
    }

    public function newSablonyFormSucceeded(Form $form, $values)
    {
        $this->SablonyManager->insertSablonu($values);
        // ...
        $this->flashMessage('Šablona byla vytvořena.', 'alert alert-success');
        $this->redirect('Sablony:');
    }


    protected function createComponentEditSablonyForm()
    {
        $sablony = $this->SablonyManager->getSablonuById($this->getParam("id"));
        $form = new Form;
        $form->addText('nazev', 'Název kurzu:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($sablony->nazev);

        $form->addTextArea('kratky_popis', 'Kártký popis:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($sablony->kratky_popis);

        $form->addTextArea('popis', 'Detailní popis:')
             ->setAttribute('id', 'editor-textu')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($sablony->popis);  

        $form->addTextArea('sylab_popis', 'Popis sylabu:')
        ->setAttribute('id', 'editor-textu2')
        ->setAttribute('req required', 'required')
        ->setDefaultValue($sablony->sylab_popis); 

        $form->addTextArea('cil_predmetu', 'Cíl předmětu:')
        ->setAttribute('id', 'editor-textu3')
        ->setAttribute('req required', 'required')
        ->setDefaultValue($sablony->cil_predmetu);                   

        $form->addTextArea('obsah', 'Obsah:')
        ->setAttribute('id', 'editor-textu4')
        ->setAttribute('req required', 'required')
        ->setDefaultValue($sablony->obsah); 
 
        $form->addTextArea('garanti_vyucujici_popis', 'Garanti vyučující popis:')
        ->setAttribute('id', 'editor-textu5')
        ->setAttribute('req required', 'required')
        ->setDefaultValue($sablony->garanti_vyucujici_popis);

        $form->addTextArea('literatura', 'Literatura:')
        ->setAttribute('id', 'editor-textu6')
        ->setAttribute('req required', 'required')
        ->setDefaultValue($sablony->literatura);

        $form->addTextArea('pozadavky_na_studenta', 'Požadavky na studenta:')
        ->setAttribute('id', 'editor-textu7')
        ->setAttribute('req required', 'required')
        ->setDefaultValue($sablony->pozadavky_na_studenta);

        $form->addText('pracoviste_zkratka', 'Zkratka pracoviště:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($sablony->pracoviste_zkratka);  

        $form->addText('rozsah_hodin', 'Rozsah hodin:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setAttribute('data-inputmask','\'mask\': \'99 - 99\'')
             ->setDefaultValue($sablony->rozsah_hodin); 

$zakonceni_kurzu = [
    'Zápočet' => 'Zápočet',
    'Zkouška' => 'Zkouška',
];

$form->addSelect('zakonceni_kurzu', 'Zakončení kurzu:', $zakonceni_kurzu)
    ->setAttribute('class', 'form-control')
    ->setAttribute('req required', 'required')
    ->setPrompt('Vyberte zakončení kurzu...')
    ->setDefaultValue($sablony->zakonceni_kurzu);

$vyucovaci_metoda = [
    'Prezenční' => 'Prezenční',
    'Kombinovaná' => 'Kombinovaná',
    'Dálková' => 'Dálková',    
];

$form->addSelect('vyucovaci_metoda', 'Vyučovací metoda:', $vyucovaci_metoda)
    ->setAttribute('class', 'form-control')
    ->setAttribute('req required', 'required')
    ->setPrompt('Vyberte vyučovací metodu...')
    ->setDefaultValue($sablony->vyucovaci_metoda);  

$form->addHidden('id', $sablony->id_kurz);

        $form->addSubmit('pridat', 'Upravit šablonu kurzu');
        $form->onSuccess[] = [$this, 'editSablonyFormSucceeded'];
        return $form;
    }


    public function editSablonyFormSucceeded(Form $form, $values)
    {
        $id = $values->id;
        unset($values->id);
        $this->SablonyManager->updateSablony($id, $values);
        $this->flashMessage('Šablona byla upravena.', 'alert alert-success');
        $this->redirect('Sablony:');
    }


}
