<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\SablonyManager;
use App\Model\LokaceManager;
use App\Model\BehManager;
use App\Model\UserManager;
use App\Model\BehDatumManager;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Security\Passwords;
use Nette\Utils\Strings;
use Tracy\Debugger;




class HodinyPresenter extends BasePresenter
{

	/** @var HodinyPresenter */
    private $SablonyManager;
    private $LokaceManager;
    private $BehManager;  
    private $BehDatumManager;
    public $UserManager;         

    public function __construct(SablonyManager $SablonyManager, LokaceManager $LokaceManager, BehManager $BehManager, BehDatumManager $BehDatumManager, UserManager $UserManager)
    {
        $this->SablonyManager = $SablonyManager;
        $this->LokaceManager = $LokaceManager;
        $this->BehManager = $BehManager;
        $this->BehDatumManager = $BehDatumManager;  
        $this->UserManager = $UserManager;
                    
    }


    public function renderDefault()
    {
        $this->template->behy = $this->BehManager->getAllBehy();
        $this->template->now = new \Nette\Utils\DateTime();

        
    }

    public function renderInformace($id)
    {
        $this->template->kurz = $this->BehManager->getBehById($id);
        $this->template->now = new \Nette\Utils\DateTime();
    }


    public function getDateFormat($date){
    	return date("j. n. Y", $date);
    }


    public function renderSchvalit()
    {
        $this->template->usersnotactive = $this->UserManager->getAllUsersNotActive();   
    }

    public function handlePrihlasitSe($id){
        $hodnoty = new \Nette\Utils\ArrayHash;
        $castka = new \Nette\Utils\ArrayHash;
        $hodnoty->beh_beh_id = $id;
        $hodnoty->uzivatel_id_ucastnik = $this->getUser()->id;
        $hodnoty->lektor = 0;
        $hodnoty->splneno = 0;
        $beh = $this->BehManager->getBehById($id);
        $stav_konta = $this->UserManager->getUserById($this->getUser()->id);
        $castka->stav_konta = ($stav_konta->stav_konta - $beh->castka);
        $this->KurzPrihlaseniManager->insertKurzPrihlaseni($hodnoty);
        $this->UserManager->updateUzivatel($this->getUser()->id, $castka);
        $this->flashMessage('Byl jste úspěšně přihlášen na kurz!', 'alert alert-success');
        $this->redirect('Hodiny:');

    }

    public function handleOdhlasitSe($id){
        $this->KurzPrihlaseniManager->deleteKurzPrihlaseni($id, $this->getUser()->id);
        $castka = new \Nette\Utils\ArrayHash;
        $beh = $this->BehManager->getBehById($id);
        $stav_konta = $this->UserManager->getUserById($this->getUser()->id);
        $castka->stav_konta = ($stav_konta->stav_konta + $beh->castka);
        $this->UserManager->updateUzivatel($this->getUser()->id, $castka);
        $this->flashMessage('Byl jste odhlášen z kurzu', 'alert alert-danger');
        $this->redirect('Hodiny:');
    }


    public function jePrihlasen($id){
        return $this->KurzPrihlaseniManager->jePrihlasen($this->getUser()->id, $id) != NULL;
    }




    public function getPocetLidi($id){
        return $this->BehManager->getPocetLidiVBehu($id);
    }

    public function jeLektor($id){
        return $this->KurzPrihlaseniManager->jeLektor($this->getUser()->id, $id) != NULL;
    }


	public function renderUpravit($id)
    {
        $this->template->getLatte()->addProvider('formsStack', [$this['editBehForm']]);         
    }

    public function renderPridat(){
        $this->template->getLatte()->addProvider('formsStack', [$this['newBehForm']]);    
    }


    private function vypisJmenoPrijmeni($array){
    $return = array();
    foreach ($array as $a) {
        $return[$a->id_ucastnik] = $a->jmeno . " " . $a->prijmeni;
    }
    return $return;
    }


        protected function createComponentNewBehForm()
    {
        $form = new Form;

        $vypisSablon = $this->SablonyManager->getAllSablony()->fetchpairs('id_kurz', 'nazev');

        $form->addSelect('kurz_id_kurz', 'Kurz:', $vypisSablon)
        ->setAttribute('class', 'form-control')
        ->setPrompt('Vyberte kurz, který chcete spustit...');

        $form->addText('rok', 'Rok:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setAttribute('data-inputmask','\'mask\': \'9999\'');
    
    $semestrInfo = [
    'LS' => 'Letní semestr',
    'ZS' => 'Zimní semestr',
    ];

        $form->addSelect('semestr', 'Semestr:', $semestrInfo)
        ->setAttribute('class', 'form-control')
        ->setPrompt('Vyberte semestr...'); 

        $form->addText('castka', 'Cena kurzu:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required');
        $form->addText('zacatek_kurzu', 'Začátek kurzu:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required datumcisty has-feedback-left')
             ->setAttribute('req required', 'required');
        $form->addText('konec_kurzu', 'Konec kurzu:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required datumcisty has-feedback-left')
             ->setAttribute('aria-describedby', 'inputSuccess2Status')
             ->setAttribute('req required', 'required');



$terminy = [
    '1' => 'Kurz s vlastními termíny',
    '2' => 'Kurz s pravidelnými termíny',    
];

$form->addRadioList('termin', 'Možnost termínů kurzů:', $terminy)
    ->setDefaultValue('1')
     ->addCondition($form::EQUAL, '1')
        ->toggle('terminyUrcite')
     ->elseCondition($form::EQUAL, '2')
        ->toggle('terminyPravidelne')
    ->endCondition();   


$behLektor = $form->addDynamic('behLektor', function (Container $behLektor) {

$vypisLektoru = $this->vypisJmenoPrijmeni($this->UserManager->getAllUsers()->where('role', 'Uživatel'));
            $behLektor->addSelect('uzivatel_id_ucastnik', 'Lektor:', $vypisLektoru)
            ->setAttribute('class', 'form-control')
            ->setPrompt('Vyberte profesora...');




            $behLektor->addSubmit('remove', 'Odstranit lektora')
                ->setValidationScope(FALSE)->setAttribute('class', 'btn btn-danger ajax')->addRemoveOnClick(array($this, "prekreslitLektora"));
        },1);
        $behLektor->addSubmit('add', 'Přidat dalšího lektora')
            ->setValidationScope(FALSE)->setAttribute('class', 'btn btn btn-success ajax')->addCreateOnClick(TRUE, array($this, "prekreslitLektora"));


$terminyUrcite = $form->addDynamic('terminyUrcity', function (Container $terminyUrcite) {
            $terminyUrcite->addText('nazev', 'Název termínu:')
                ->setAttribute('class', 'form-control');
$vypisLokaci = $this->LokaceManager->getAllLokace()->fetchpairs('id_lokace', 'nazev');
            $terminyUrcite->addSelect('lokace_id_lokace', 'Lokace:', $vypisLokaci)
            ->setAttribute('class', 'form-control')
            ->setPrompt('Vyberte lokaci, který chcete spustit...');


            $terminyUrcite->addText('date_ucasti', 'Datum a čas:')
                ->setAttribute('class', 'form-control has-feedback-left datumcas');




            $terminyUrcite->addSubmit('remove', 'Odstranit termín')
                ->setValidationScope(FALSE)->setAttribute('class', 'btn btn-danger ajax')->addRemoveOnClick(array($this, "prekreslitTermin"));
        },0);
        $terminyUrcite->addSubmit('add', 'Přidat další termín')
            ->setValidationScope(FALSE)->setAttribute('class', 'btn btn btn-success ajax')->addCreateOnClick(TRUE, array($this, "prekreslitTermin"));




$terminyPravidelne = $form->addDynamic('terminyPravidelny', function (Container $terminyPravidelne) {
            $terminyPravidelne->addText('nazev', 'Název termínu:')
                ->setAttribute('class', 'form-control');
$vypisLokaci = $this->LokaceManager->getAllLokace()->fetchpairs('id_lokace', 'nazev');
            $terminyPravidelne->addSelect('lokace_id_lokace', 'Lokace:', $vypisLokaci)
            ->setAttribute('class', 'form-control')
            ->setPrompt('Vyberte lokaci, který chcete spustit...');


    $denVTydnu = [
    'Monday' => 'Každé pondělí',
    'Tuesday' => 'Každé úterý',
    'Wednesday' => 'Každou středu',
    'Thursday' => 'Každý čtvrtek', 
    'Friday' => 'Každý pátek', 
    'Saturday' => 'Každou sobotu', 
    'Sunday' => 'Každou neděli',                    
    ];

        $terminyPravidelne->addSelect('datumTyden', 'Pro den v týdenu:', $denVTydnu)
        ->setAttribute('class', 'form-control')
        ->setPrompt('Vyberte den pro opakování...'); 


    $rozhodnutiSudyLichy = [
    'tyden' => 'Každé týden',
    'sudy' => 'Sudý týden',
    'lichy' => 'Lichý týden',                   
    ];

        $terminyPravidelne->addSelect('oprakovaniTyden', 'Jak často v týdnu:', $rozhodnutiSudyLichy)
        ->setAttribute('class', 'form-control')
        ->setPrompt('Vyberte jak často pro týden...');


            $terminyPravidelne->addText('cas', 'Čas ve formátu (XX:XX):')
                ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
                ->setAttribute('data-inputmask','\'mask\': \'99:99\'');


            $terminyPravidelne->addSubmit('remove', 'Odstranit termín')
                ->setValidationScope(FALSE)->setAttribute('class', 'btn btn-danger ajax')->addRemoveOnClick(array($this, "prekreslitPravidelnyTermin"));
        },0);
        $terminyPravidelne->addSubmit('add', 'Přidat další termín')
            ->setValidationScope(FALSE)->setAttribute('class', 'btn btn btn-success ajax')->addCreateOnClick(TRUE, array($this, "prekreslitPravidelnyTermin"));





        $form->addSubmit('pridat', 'Spustit kurz');
        $form->onSuccess[] = [$this, 'newBehFormSucceeded'];
        return $form;
    }


    public function newBehFormSucceeded(Form $form, $values)
    {

        $beh = new Nette\ArrayHash;

        $beh->kurz_id_kurz = $values->kurz_id_kurz;
        

        $beh->rok = $values->rok;
        unset($values->rok);

        $beh->semestr = $values->semestr;
        unset($values->semestr); 

        $beh->castka = $values->castka;
        unset($values->castka);

        $beh->zacatek_kurzu = $values->zacatek_kurzu;
        unset($values->zacatek_kurzu);

        $beh->konec_kurzu = $values->konec_kurzu;
        unset($values->konec_kurzu);
        $this->BehManager->insertBeh($beh);

        $vlzenoid = $this->BehManager->getLastId();


        foreach ($values->behLektor as $lektor) {
                $lektor->beh_beh_id = $vlzenoid;
                $lektor->splneno = false;
                $lektor->lektor = true;
                $this->KurzPrihlaseniManager->insertKurzPrihlaseni($lektor);
         } 


        if($values->termin == "1"){
            //zapis vlastních termínů
            foreach ($values->terminyUrcity as $termin) {
                
                $termin->kurz_id_kurz = $values->kurz_id_kurz;
                $termin->beh_beh_id = $vlzenoid;
                $this->BehDatumManager->insertBehDatum($termin);

         } 


        } elseif($values->termin == "2"){
            //zápis opakovatelných termínů
                
           foreach ($values->terminyPravidelny as $termin) {
            $termin->beh_beh_id = $vlzenoid;
            $termin->kurz_id_kurz = $values->kurz_id_kurz;
            $nazev = $termin->nazev;
            $lokace_id_lokace = $termin->lokace_id_lokace;
            $datumTyden = $termin->datumTyden;
            $oprakovaniTyden = $termin->oprakovaniTyden;
            $cas = $termin->cas;

unset($termin->oprakovaniTyden);
unset($termin->datumTyden);
unset($termin->cas);


            $startdate = $beh->zacatek_kurzu;
            $endDate = $beh->konec_kurzu;
$startSekundy = strtotime($startdate);
$endSekundy = strtotime($endDate);

        for($i = $startSekundy; $i <= $endSekundy; $i = $i + 86400){
                $denNazev = date('l', $i); 
                $cisloTyden = StrFTime("%U",  $i);
                $datumFormat = date('Y-m-d', $i); 
                $datumProVlozeni = $datumFormat . " " . $cas . ":00";
                $termin->date_ucasti = $datumProVlozeni;
                if($datumTyden == $denNazev){
                    

                    if($oprakovaniTyden == "tyden"){

                        $this->BehDatumManager->insertBehDatum($termin);
                    }elseif($oprakovaniTyden == "sudy"){
                        if($this->jeLiche($cisloTyden) == false){
                            $this->BehDatumManager->insertBehDatum($termin);
                        }
                    }elseif($oprakovaniTyden == "lichy"){
                        if($this->jeLiche($cisloTyden) == true){
                            $this->BehDatumManager->insertBehDatum($termin);
                        }
                    }

                    }
                }
            }

         }  

         $this->flashMessage('Kurz byl spuštěn.', 'alert alert-success');
        $this->redirect('Beh:');
    }


protected function createComponentEditBehForm()
    {

        $id = $this->getParam("id");
        $beh = $this->BehManager->getBehById($id);
        $beh_datumy = $this->BehDatumManager->getBehDatumById($id);
        $kurzLektor = $this->KurzPrihlaseniManager->getPrihlaseniByIdKurz($id);


        $form = new Form($this, "editBehForm");

        $vypisSablon = $this->SablonyManager->getAllSablony()->fetchpairs('id_kurz', 'nazev');

        $form->addSelect('kurz_id_kurz', 'Kurz:', $vypisSablon)
        ->setAttribute('class', 'form-control')
        ->setPrompt('Vyberte kurz, který chcete spustit...')
        ->setDefaultValue($beh->kurz_id_kurz);


        $form->addText('rok', 'Rok:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setAttribute('data-inputmask','\'mask\': \'9999\'')
             ->setDefaultValue($beh->rok);
    
    $semestrInfo = [
    'LS' => 'Letní semestr',
    'ZS' => 'Zimní semestr',
    ];

        $form->addSelect('semestr', 'Semestr:', $semestrInfo)
        ->setAttribute('class', 'form-control')
        ->setPrompt('Vyberte semestr...')
        ->setDefaultValue($beh->semestr);

        $form->addText('castka', 'Cena kurzu:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required has-feedback-left')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($beh->castka);
        $form->addText('zacatek_kurzu', 'Začátek kurzu:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required datumcisty has-feedback-left')
             ->setAttribute('aria-describedby', 'inputSuccess2Status')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($beh->zacatek_kurzu);
        $form->addText('konec_kurzu', 'Konec kurzu:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required datumcisty has-feedback-left')
             ->setAttribute('aria-describedby', 'inputSuccess2Status')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($beh->konec_kurzu);

        $form->addHidden('id', $id);


$behLektor = $form->addDynamic('behLektor', function (Container $behLektor) {

$vypisLektoru = $this->vypisJmenoPrijmeni($this->UserManager->getAllUsers()->where('role', 'Uživatel'));
            $behLektor->addSelect('uzivatel_id_ucastnik', 'Lektor:', $vypisLektoru)
            ->setAttribute('class', 'form-control')
            ->setPrompt('Vyberte profesora...');
            $behLektor->addHidden('ucastnik_has_beh_id');




            $behLektor->addSubmit('remove', 'Odstranit lektora')
                ->setValidationScope(FALSE)->setAttribute('class', 'btn btn-danger ajax')->addRemoveOnClick(array($this, "prekreslitLektora"));
        },1);
        $behLektor->addSubmit('add', 'Přidat dalšího lektora')
            ->setValidationScope(FALSE)->setAttribute('class', 'btn btn btn-success ajax')->addCreateOnClick(TRUE, array($this, "prekreslitLektora"));

 if (!$form->isSubmitted()) {
            if (count($kurzLektor)) {
                $default_lektor = array();
                foreach ($kurzLektor as $lektor) {
                    $default_lektor[] = ['uzivatel_id_ucastnik'=>$lektor->uzivatel_id_ucastnik, 'ucastnik_has_beh_id'=>$lektor->ucastnik_has_beh_id];
               
                }
                $behLektor->setDefaults($default_lektor);
            }
        }




$terminyUrcite = $form->addDynamic('terminyUrcity', function (Container $terminyUrcite) {
            $terminyUrcite->addText('nazev', 'Název termínu:')
                ->setAttribute('class', 'form-control');
$vypisLokaci = $this->LokaceManager->getAllLokace()->fetchpairs('id_lokace', 'nazev');
            $terminyUrcite->addSelect('lokace_id_lokace', 'Lokace:', $vypisLokaci)
            ->setAttribute('class', 'form-control')
            ->setPrompt('Vyberte lokaci, který chcete spustit...');


            $terminyUrcite->addText('date_ucasti', 'Datum a čas:')
                ->setAttribute('class', 'form-control datumcas has-feedback-left');
            $terminyUrcite->addHidden('idbeh_datum');




            $terminyUrcite->addSubmit('remove', 'Odstranit termín')
                ->setValidationScope(FALSE)->setAttribute('class', 'btn btn-danger ajax')->addRemoveOnClick(array($this, "prekreslitTermin"));
        },0);
        $terminyUrcite->addSubmit('add', 'Přidat další termín')
            ->setValidationScope(FALSE)->setAttribute('class', 'btn btn btn-success ajax')->addCreateOnClick(TRUE, array($this, "prekreslitTermin"));


        if (!$form->isSubmitted()) {
            if (count($beh_datumy)) {
                $default_terminy = array();
                foreach ($beh_datumy as $ter) {
                    $default_terminy[] = ['nazev'=>$ter->nazev, 'lokace_id_lokace'=>$ter->lokace_id_lokace, 'date_ucasti' => $ter->date_ucasti, 'idbeh_datum' => $ter->idbeh_datum];
               
                }
                $terminyUrcite->setDefaults($default_terminy);
            }
        }


        $form->addSubmit('pridat', 'Upravit kurz');
        $form->onSuccess[] = [$this, 'editBehFormSucceeded'];
        return $form;
    }

    public function editBehFormSucceeded(Form $form, $values)
    {
        $id = $values->id;
        $beh = new Nette\ArrayHash;
        $beh->kurz_id_kurz = $values->kurz_id_kurz;
        $beh->rok = $values->rok;
        unset($values->rok);
        $beh->semestr = $values->semestr;
        unset($values->semestr); 
        $beh->castka = $values->castka;
        unset($values->castka);
        $beh->zacatek_kurzu = $values->zacatek_kurzu;
        unset($values->zacatek_kurzu);
        $beh->konec_kurzu = $values->konec_kurzu;
        unset($values->konec_kurzu);
        $this->BehManager->updateBeh($id ,$beh);

        $vsechnyLektori = $this->KurzPrihlaseniManager->getPrihlaseniByIdKurz($id);

        //při odeslání formuláře zkontroluje, jestli uživatel nechce smazat některé telefony, že je prostě odstranil
        foreach ($vsechnyLektori as $lektori) {
            $check = false;
            foreach ($values->behLektor as $l) {
                

                if($lektori->uzivatel_id_ucastnik == $l->uzivatel_id_ucastnik){
                    $check = true;
                    
                }



            }   
            if ($check == false) {
                $this->KurzPrihlaseniManager->deleteKurzPrihlaseni($lektori->ucastnik_has_beh_id); 
 
            }
        }



        foreach ($values->behLektor as $b) {
            if($b->ucastnik_has_beh_id == ""){
                $b->beh_beh_id = $id;
                $b->lektor = true;
               $this->KurzPrihlaseniManager->insertKurzPrihlaseni($b);
                
            }
            else {
                $beh_id = $b->ucastnik_has_beh_id;
                unset($b->ucastnik_has_beh_id);
               $this->KurzPrihlaseniManager->updateKurzPrihlaseni($beh_id, $b);
                
            }
        }
            

        $vsechnyBehyDatumy = $this->BehDatumManager->getBehDatumById($id);

        //při odeslání formuláře zkontroluje, jestli uživatel nechce smazat některé telefony, že je prostě odstranil
        foreach ($vsechnyBehyDatumy as $datumy) {
            $check = false;
            foreach ($values->terminyUrcity as $t) {
                

                if($datumy->idbeh_datum == $t->idbeh_datum){
                    $check = true;
                }

            }   
            if ($check == false) {
                $this->BehDatumManager->deleteBehDatum($datumy->idbeh_datum);  
            }
        }



        foreach ($values->terminyUrcity as $t) {
            if($t->idbeh_datum == ""){
                $t->beh_beh_id = $id;
                $t->kurz_id_kurz = $beh->kurz_id_kurz;
               $this->BehDatumManager->insertBehDatum($t);
                
            }
            else {
                $beh_id = $t->idbeh_datum;
                unset($t->idbeh_datum);
               $this->BehDatumManager->updateBehDatum($beh_id, $t);
                
            }
        }

        $this->flashMessage('Kurz byl upraven.', 'alert alert-success');
        $this->redirect('Beh:');
    }







    public function jeLiche($cislo){
        if($cislo % 2 == 0){
            return false;
        }else{
            return true;
        }
    }



	public function startup()
	{
		parent::startup();
		if($this->getUser()->isLoggedIn()){
            if (!$this->user->isAllowed($this->name, $this->action)){
                $this->flashMessage('Nemáte oprávnění pro přístup do kurzů!', 'alert alert-danger');
                $this->redirect("Homepage:");
            }
		}else{
			$this->redirect("Sign:in");
		}
	}

public function prekreslitPravidelnyTermin()
{
    if ($this->isAjax()) {
        $this->redrawControl('terminyPravidelne');
        $this->redrawControl('scriptyDatePicker');
    }
}

public function prekreslitTermin()
{
    if ($this->isAjax()) {
        $this->redrawControl('terminyUrcite');
        $this->redrawControl('scriptyDatePicker');
    }
}

public function prekreslitLektora()
{
    if ($this->isAjax()) {
        $this->redrawControl('behLektori');
    }
}



}
