<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\UserManager;
use App\Model\TelefonManager;
use App\Model\AdresaManager;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Security\Passwords;
use Nette\Utils\Strings;
use Tracy\Debugger;




class ProfilPresenter extends BasePresenter
{

	/** @var UserManager */
    public $UserManager;
    private $TelefonManager;
    private $AdresaManager;


    public function __construct(UserManager $UserManager, TelefonManager $TelefonManager, AdresaManager $AdresaManager)
    {
        $this->TelefonManager = $TelefonManager;
        $this->UserManager = $UserManager;
        $this->AdresaManager = $AdresaManager;
    }


    public function renderDefault()
    {
        $this->template->getLatte()->addProvider('formsStack', [$this['editUzivatelForm']]);
    }

    public function renderSchvalit()
    {
        $this->template->usersnotactive = $this->UserManager->getAllUsersNotActive();   
    }


	public function renderUpravit($id)
    {
        $this->template->getLatte()->addProvider('formsStack', [$this['editUzivatelForm']]);         
    }



	public function startup()
	{
		parent::startup();
		if($this->getUser()->isLoggedIn()){
            if (!$this->user->isAllowed($this->name, $this->action)){
                $this->flashMessage("Nemáš přístup!");
                $this->redirect("Homepage");
            }
		}else{
			$this->redirect("Sign:in");
		}
	}

        protected function createComponentEditUzivatelForm()
    {
        $id = $this->user->getIdentity()->id;
        $uzivatel = $this->UserManager->getUserById($id);
        $telefony = $this->TelefonManager->getTelefonByUserId($id);
        $adresa_data = $this->AdresaManager->getAdresaByUserId($id);

        $form = new Form($this, "editUzivatelForm");
        $form->addText('jmeno', 'Jméno:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($uzivatel->jmeno);

        $form->addText('prijmeni', 'Příjmení:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($uzivatel->prijmeni);

        $form->addText('titul_pred', 'Titul před jménem:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setDefaultValue($uzivatel->titul_pred);      

        $form->addText('titul_za', 'Titul za jménem:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setDefaultValue($uzivatel->titul_za);

        $form->addText('username', 'Uživatelské jméno:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setAttribute('req required', 'required')
             ->setDisabled(TRUE)
             ->setDefaultValue($uzivatel->username); 

        $form->addPassword('password', 'Heslo:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required');

        $form->addPassword('password2', 'Heslo znova:')
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required');        

        $form->addText('email', 'E-mail:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setAttribute('req required', 'required')
             ->setDefaultValue($uzivatel->email);

        $form->addHidden('id', $id);
       

$zakonceni_kurzu = [
    'Uživatel' => 'Uživatel',
    'Administrátor' => 'Administrátor',
];

        $form->addSelect('role', 'Oprávnění:', $zakonceni_kurzu)
        ->setAttribute('class', 'form-control')
        ->setDisabled(TRUE)
        ->setPrompt('Vyberte oprávnění...')
        ->setDefaultValue($uzivatel->role);

        $form->addText('stav_konta', 'Stav konta:') 
             ->setAttribute('class', 'form-control col-md-7 col-xs-12 required')
             ->setAttribute('req required', 'required')
             ->setDisabled(TRUE)
             ->setDefaultValue($uzivatel->stav_konta);    

$form->addUpload('image', 'Obrázek:')
    ->setRequired(FALSE) // nepovinný
    ->addRule(Form::IMAGE, 'Avatar musí být JPEG, PNG nebo GIF.')
    ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 1 Mb.', 1024 * 1024 /* v bytech */);


$zakonceni_kurzu = [
    1 => 'Schváleno',
    0 => 'Neschváleno',
    2 => 'Zamítnuto',
];

        $form->addSelect('aktivni', 'Aktivní účet:', $zakonceni_kurzu)
        ->setAttribute('class', 'form-control')
        ->setPrompt('schválení uživatele...')
        ->setDisabled(TRUE)
        ->setDefaultValue($uzivatel->aktivni); 



$telefon = $form->addDynamic('telefony', function (Container $telefon) {
            $telefon->addText('nazev', 'Název kontaktu:')
                ->setAttribute('class', 'form-control')
                ->setRequired('Zadejte prosím telefon.');
            $telefon->addText('telefon', 'Telefoní číslo:')
                ->setAttribute('class', 'form-control')
                ->setRequired('Zadejte prosím telefon.');
            $telefon->addHidden('id_telefon');
            $telefon->addSubmit('remove', 'Odstranit telefon')
                ->setValidationScope(FALSE)->setAttribute('class', 'btn btn-danger ajax')->addRemoveOnClick(array($this, "prekreslitTelefon"));
        },0);
        $telefon->addSubmit('add', 'Přidat další telefon')
            ->setValidationScope(FALSE)->setAttribute('class', 'btn btn btn-success ajax')->addCreateOnClick(TRUE, array($this, "prekreslitTelefon"));
  
        if (!$form->isSubmitted()) {
            if (count($telefony)) {
                $default_phone = array();
                foreach ($telefony as $phone) {
                    $default_phone[] = ['nazev'=>$phone->nazev, 'telefon'=>$phone->telefon, 'id_telefon' => $phone->id_telefon];

                }
                $telefon->setDefaults($default_phone);
            }
        }
            



$adresa = $form->addDynamic('adresy', function (Container $adresa) {
            $adresa->addText('nazev', 'Název kontaktu:')
                ->setAttribute('class', 'form-control')
                ->setRequired('Zadejte prosím telefon.');
            $adresa->addText('ulice', 'Ulice:')
                ->setAttribute('class', 'form-control')
                ->setRequired('Zadejte prosím telefon.');
            $adresa->addText('cislo_popisne', 'Číslo popisné:')
                ->setAttribute('class', 'form-control')
                ->setRequired('Zadejte prosím telefon.');
            $adresa->addText('mesto', 'Město:')
                ->setAttribute('class', 'form-control')
                ->setRequired('Zadejte prosím telefon.');
            $adresa->addText('psc', 'PSČ:')
                ->setAttribute('class', 'form-control')
                ->setRequired('Zadejte prosím telefon.');
            $adresa->addHidden('id_ucastnikAdresa');


            $adresa->addSubmit('remove', 'Odstranit adresu')
                ->setValidationScope(FALSE)->setAttribute('class', 'btn btn-danger ajax')->addRemoveOnClick(array($this, "prekreslitAdresu"));
        },1);
        $adresa->addSubmit('add', 'Přidat další adresu')
            ->setValidationScope(FALSE)->setAttribute('class', 'btn btn btn-success ajax')->addCreateOnClick(TRUE, array($this, "prekreslitAdresu"));


        if (!$form->isSubmitted()) {
            if (count($adresa_data)) {
                $default_adresa = array();
                foreach ($adresa_data as $adresa_tmp) {
                    $default_adresa[] = ['ulice'=>$adresa_tmp->ulice, 'cislo_popisne'=>$adresa_tmp->cislo_popisne, 'mesto' => $adresa_tmp->mesto, 'psc' => $adresa_tmp->psc, 'nazev' => $adresa_tmp->nazev, 'id_ucastnikAdresa' => $adresa_tmp->id_ucastnikAdresa];

                }
                $adresa->setDefaults($default_adresa);
            }
        }


        $form->addSubmit('pridat', 'Upravit profil');
        $form->onSuccess[] = [$this, 'editUzivatelFormSucceeded'];
        return $form;
    }


public function editUzivatelFormSucceeded(Form $form, $values)
    {
        $id = $values->id;
        unset($values->id);

        $uzivatel = new Nette\ArrayHash;

        $uzivatel->jmeno = $values->jmeno;
        unset($values->jmeno);

        $uzivatel->prijmeni = $values->prijmeni;
        unset($values->prijmeni);

        $uzivatel->titul_pred = $values->titul_pred;
        unset($values->titul_pred); 

        $uzivatel->titul_za = $values->titul_za;
        unset($values->titul_za);


        if($values->password == "" || $values->password == NULL){

        }else{
        $uzivatel->password = Passwords::hash($values->password);
        unset($values->password);    
        }

        

        $uzivatel->email = $values->email;
        unset($values->email);



        if($values->image->isOk()) {
            $imageArray = $this->UserManager->getParametrById('image', $id);
            if($imageArray->image != NULL){
            $this->imageStorage->setNamespace("profile")->deleteFile($imageArray->image);
            }
            $fileName = Nette\Utils\Random::generate(10) . "_" . Strings::webalize($values->image->getName(), '.');
            $this->imageStorage->setNamespace("profile")->upload($values->image, $fileName);
            $uzivatel->image = $fileName;
            unset($values->image);
        }

        $this->UserManager->updateUzivatel($id, $uzivatel);

$vsechnyTelefony = $this->TelefonManager->getTelefonByUserId($id);

//při odeslání formuláře zkontroluje, jestli uživatel nechce smazat některé telefony, že je prostě odstranil
foreach ($vsechnyTelefony as $telefon) {
            $check = false;
            foreach ($values->telefony as $t) {
                
                if($t->id_telefon == $telefon->id_telefon){
                    $check = true;
                }

            }   
            if ($check == false) {
                $this->TelefonManager->deleteTelefon($telefon->id_telefon);
               
            }
        }


        foreach ($values->telefony as $t) {
            if($t->id_telefon == ""){
                $t->uzivatel_id_ucastnik = $id;
                $this->TelefonManager->insertTelefon($t);
                
            }
            else {
                $telefon_id = $t->id_telefon;
                unset($t->id_telefon);
                $this->TelefonManager->updateTelefon($telefon_id, $t);
                
            }
        }

$vsechnyAdresy = $this->AdresaManager->getAdresaByUserId($id);
foreach ($vsechnyAdresy as $adresa) {
            $check = false;
            foreach ($values->adresy as $a) {
               
                if($a->id_ucastnikAdresa == $adresa->id_ucastnikAdresa){
                    $check = true;
                }
            }   
            if ($check == false) {
                $this->AdresaManager->deleteAdresa($adresa->id_ucastnikAdresa);
                
            }
        }


        foreach ($values->adresy as $a) {
            if($a->id_ucastnikAdresa == ""){
                $a->uzivatel_id_ucastnik = $id;
                $this->AdresaManager->insertAdresu($a);
                
            }
            else {
                $adresa_id = $a->id_ucastnikAdresa;
                unset($a->id_ucastnikAdresa);
                $this->AdresaManager->updateAdresa($adresa_id, $a);
            }
        }

        $this->flashMessage('Profil byl upraven.');
       $this->redirect('Profil:');
    }


public function prekreslitTelefon()
{
    if ($this->isAjax()) {
        $this->redrawControl('telefon');
    }
}

public function prekreslitAdresu()
{
    if ($this->isAjax()) {
        $this->redrawControl('adresa');
    }
}


}
