<?php

namespace App\Presenters;

use Nette;
use App\Forms;
use App\Model\LogManager;
use App\Model\UserManager;

class SignPresenter extends BasePresenter
{
	/** @var Forms\SignInFormFactory @inject */
	public $signInFactory;

	/** @var Forms\SignUpFormFactory @inject */
	public $signUpFactory;

	private $LogManager;


	public function __construct(LogManager $LogManager)
	{
		$this->LogManager = $LogManager;
	}

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		return $this->signInFactory->create(function () {

			$this->redirect('Homepage:');
		});
	}

	/**
	 * Sign-up form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignUpForm()
	{		
		return $this->signUpFactory->create(function () {
			$this->flashmessage('Registrace byla úspěšná', 'alert alert-success');
			$this->redirect('Homepage:');
		});
	}




	public function actionOut()
	{
		$hodnoty = new \Nette\Utils\ArrayHash;
		$hodnoty->uzivatel_id_ucastnik = $this->user->id;
		$hodnoty->aktivita = "Odhlášení";
		$hodnoty->datetime = new \Nette\Utils\DateTime();
		$this->LogManager->insertLog($hodnoty);
		$this->getUser()->logout();
	}

}
