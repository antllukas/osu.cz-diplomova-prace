<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\UserManager;
use App\Model\BehManager;
use App\Model\BehDatumManager;
use App\Model\CertificateManager;
use App\Model\LokaceManager;
use App\Model\SablonyManager;
use App\Model\LogManager;
use App\Model\KurzPrihlaseniManager;



class HomepagePresenter extends BasePresenter
{

	/** @var UserManager */
    public $UserManager;
    private $BehManager;
    public $BehDatumManager;    
    private $CertificateManager;
    private $LokaceManager;
    private $SablonyManager;
    private $LogManager;
    public $KurzPrihlaseniManager;    

    public function __construct(UserManager $UserManager, BehManager $BehManager, CertificateManager $CertificateManager, LokaceManager $LokaceManager, SablonyManager $SablonyManager, LogManager $LogManager, KurzPrihlaseniManager $KurzPrihlaseniManager, BehDatumManager $BehDatumManager)
    {
        $this->UserManager = $UserManager;
        $this->BehManager = $BehManager;
        $this->CertificateManager = $CertificateManager;
        $this->LokaceManager = $LokaceManager;
        $this->SablonyManager = $SablonyManager;
        $this->LogManager = $LogManager;
        $this->KurzPrihlaseniManager = $KurzPrihlaseniManager;
        $this->BehDatumManager = $BehDatumManager;
    }

	public function renderDefault()
	{
		$this->template->users = $this->UserManager->getAllUsers();
		$this->template->test = $this->UserManager->getUserById(1);
		$this->template->pocetUzivatelu = $this->UserManager->getCountAllUsers();
		$this->template->pocetUzivateluSchvalit = $this->UserManager->getCountCekajiciSchvalit();
		$this->template->pocetAktivnichBehu = $this->BehManager->getPocetAktivnichBehu();
		$this->template->pocetVygenerovanychCertifikatu = $this->CertificateManager->getAllCertificateCount();
		$this->template->pocetLokaci = $this->LokaceManager->getAllLokaceCount();
		$this->template->pocetSablon = $this->SablonyManager->getAllSablonyCount();
		$this->template->posledniPrihlaseni = $this->LogManager->getAll5Prihlaseni();
		$this->template->posledniRegistrace = $this->LogManager->getAll5Registrace();
		$this->template->posledniOdhlaseni = $this->LogManager->getAll5Odhlaseni();
		$this->template->poslednich5Mesicu = $this->LogManager->get5PoslednichMesicu();
		$this->template->poslednich5MesicuPocetPrihlaseni = $this->LogManager->get5PoslednichMesicuPocetPrihlaseni();	
		$this->template->poslednich5MesicuPocetOdhlaseni = $this->LogManager->get5PoslednichMesicuPocetOdhlaseni();
		$this->template->poslednich5MesicuRegistrace = $this->LogManager->get5PoslednichMesicuRegistrace();	
		$this->template->behy = $this->BehManager->getAllBehy();
		$id = $this->user->getIdentity()->id;	
		$this->template->zapsano = $this->BehDatumManager->getBehDatumAllByUser($id);
		$this->template->now = new \Nette\Utils\DateTime();		
	}

	public function renderSingle($id){
		$this->template->test = $this->UserManager->getUserById($id);
	}

    public function jeLektor($id){
        return $this->KurzPrihlaseniManager->jeLektor($this->getUser()->id, $id) != NULL;
    }

    public function handleOdhlasitSe($id){
        $this->KurzPrihlaseniManager->deleteKurzPrihlaseni($id, $this->getUser()->id);
        $this->redirect('Hodiny:');
    }


    public function getPocetLidi($id){
        return $this->BehManager->getPocetLidiVBehu($id);
    }

    public function jePrihlasen($id){
        return $this->KurzPrihlaseniManager->jePrihlasen($this->getUser()->id, $id) != NULL;
    }



	public function getNameMonth($mesic){
		$aj = array("January","February","March","April","May","June","July","August","September","October","November","December");
		$cz = array("Leden","Únor","Březen","Duben","Květen","Červen","Červenec","Srpen","Září","Říjen","Listopad","Prosinec");
		$datum = str_replace($aj, $cz, date("F", mktime(0, 0, 0, $mesic, 1, 2000)));
		return $datum;
	}

	public function getDateFormat($date){
		return date("d.m.Y H:i:s", strtotime($date));
	}


	public function startup()
	{
		parent::startup();
		if($this->getUser()->isLoggedIn()){
			if (!$this->user->isAllowed($this->name, $this->action)){
                $this->flashMessage("Nemáš přístup!");
                $this->redirect("Homepage:");
            }
		}else{
			$this->redirect("Sign:in");
		}
	}



}
