<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use App\Model\UserManager;
use App\Model\KurzPrihlaseniManager;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	use \Brabijan\Images\TImagePipe;
	 /**
     * @inject
     * @var \Brabijan\Images\ImageStorage
     */
    public $imageStorage;
    public $KurzPrihlaseniManager;
	public $UserManager;


    public function injectModels(KurzPrihlaseniManager $KurzPrihlaseniManager,UserManager $UserManager)
    {
        $this->KurzPrihlaseniManager = $KurzPrihlaseniManager;
        $this->UserManager = $UserManager;                        
    }

	public function getProfileImage()
	{
		$img = $this->user->getIdentity()->image;
		if($img == NULL){
			$img = "default.jpg";
		}
	return $img;
	}

    public function jeNekdeLektor(){
        return $this->KurzPrihlaseniManager->jeNekdeLektor($this->getUser()->id) != NULL;
    }


	public function getImage($img)
	{
		if($img == NULL){
			$img = "default.jpg";
		}
	return $img;
	}

    public function getDateFormat($date){
    	return date("j. n. Y", strtotime($date));
    }

    public function getUzivatelInfo(){
    	return $this->UserManager->getUserById($this->getUser()->id);
    }


}
