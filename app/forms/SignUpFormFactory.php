<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI;
use Nette\Application\UI\Form;

use App\Model;
use App\Model\LogManager;


class SignUpFormFactory extends Nette\Object
{
	use Nette\SmartObject;

	const PASSWORD_MIN_LENGTH = 7;

	/** @var FormFactory */
	private $factory;

	/** @var Model\UserManager */
	private $userManager;

	private $LogManager;


	public function __construct(FormFactory $factory, Model\UserManager $userManager, LogManager $LogManager)
	{
		$this->factory = $factory;
		$this->userManager = $userManager;
		$this->LogManager = $LogManager;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->addText('username', 'Pick a username:')
			->setRequired('Please pick a username.')
			->setAttribute('class', 'form-control')
			->setAttribute('placeholder', 'Přihlašovací jméno');

		$form->addText('jmeno', 'Pick a username:')
			->setRequired('Please pick a username.')
			->setAttribute('class', 'form-control')
			->setAttribute('placeholder', 'Jméno');

		$form->addText('prijmeni', 'Pick a username:')
			->setRequired('Please pick a username.')
			->setAttribute('class', 'form-control')
			->setAttribute('placeholder', 'Příjmení');



		$form->addText('email', 'Your e-mail:')
			->setRequired('Please enter your e-mail.')
			->addRule($form::EMAIL)
			->setAttribute('class', 'form-control')
			->setAttribute('placeholder', 'Váš e-mail');

		$form->addPassword('password', 'Create a password:')
			->setOption('description', sprintf('at least %d characters', self::PASSWORD_MIN_LENGTH))
			->setRequired('Please create a password.')
			->addRule($form::MIN_LENGTH, NULL, self::PASSWORD_MIN_LENGTH)
			->setAttribute('class', 'form-control')
			->setAttribute('placeholder', 'Heslo');

		$form->addSubmit('send', 'Registrovat se')
		->setAttribute('class', 'btn btn-default submit');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			if(!$this->existujeUzivatel($values->username)){
			try {
				$this->userManager->add($values->username, $values->email, $values->password, $values->jmeno, $values->prijmeni);

				$hodnoty = new \Nette\Utils\ArrayHash;
				$hodnoty->uzivatel_id_ucastnik = $this->userManager->getLastId();
				$hodnoty->aktivita = "Registrace";
				$hodnoty->datetime = new \Nette\Utils\DateTime();
				$this->LogManager->insertLog($hodnoty);

			} catch (Model\DuplicateNameException $e) {
				$form->flashMessage('Username is already taken.');
				return;
			}}else{

			}
			$onSuccess();
		};
		return $form;
	}

	public function existujeUzivatel($username){
        return $this->userManager->getUserByUserName($username) != NULL;
    }

}
