<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use App\Model\LogManager;


class SignInFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private $factory;

	/** @var User */
	private $user;

	private $LogManager;


	public function __construct(FormFactory $factory, User $user, LogManager $LogManager)
	{
		$this->factory = $factory;
		$this->user = $user;
		$this->LogManager = $LogManager;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->addText('username', '')
			->setRequired('Prosím vložte přihlašovací jméno.')
			->setAttribute('placeholder', 'Přihlašovací jméno')
			->setAttribute('class', 'form-control');

		$form->addPassword('password', '')
			->setRequired('Prosím vložte heslo.')
			->setAttribute('placeholder', 'Heslo')
			->setAttribute('class', 'form-control');

		$form->addCheckbox('remember', ' Zůstat přihlášený');

		$form->addSubmit('send', 'Přihlásit se')
			->setAttribute('class', 'btn btn-default submit');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				$this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
				$this->user->login($values->username, $values->password);

				$hodnoty = new \Nette\Utils\ArrayHash;
				$hodnoty->uzivatel_id_ucastnik = $this->user->id;
				$hodnoty->aktivita = "Přihlášení";
				$hodnoty->datetime = new \Nette\Utils\DateTime();
				$this->LogManager->insertLog($hodnoty);
			} catch (Nette\Security\AuthenticationException $e) {
				$form->addError($e->getMessage());
				return;
			}
			$onSuccess();
		};
		return $form;
	}

}
