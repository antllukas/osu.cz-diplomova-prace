<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$router[] = new Route('login', 'Sign:in');	
		$router[] = new Route('odhlasit', 'Sign:out');
		$router[] = new Route('user/upravit/<id>', 'User:upravit');		
		$router[] = new Route('sablony/upravit/<id>', 'Sablony:upravit');
		$router[] = new Route('lokace/upravit/<id>', 'Lokace:upravit');
		$router[] = new Route('hodiny/informace/<id>', 'Hodiny:informace');
		$router[] = new Route('lektor/upravit/<id>', 'Lektor:upravit');		
		$router[] = new Route('<presenter>/<action>/', 'Homepage:default');
		return $router;
	}

}
